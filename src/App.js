import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.rtl.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import './App.css';


import Layout from './Viwes/Layout/Layout';
import Prodcut from './Viwes/Pages/Product/Product';
import EmployeesList from './Viwes/Pages/Employees/EmployeesList';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { useSelector } from 'react-redux';
import LoginPage from './Viwes/Pages/Account/LoginPage';
import Loading from './Viwes/Pages/Shared/Loading';
import Plans from './Viwes/Pages/Plans/Plans';
import User from './Viwes/Pages/Users/User';
import Withdraws from './Viwes/Pages/Withdraws/Withdraws';
function App() {


  const isUserOnline = useSelector(state => state.account.isUserOnline);
  const { isShowLoading } = useSelector(state => state.config)
  return (
    <div>
      <ToastContainer />
      {
        isShowLoading ? <Loading /> : <></>
      }

      {
        isUserOnline ? <BrowserRouter>
          <Routes>
            <Route path="/" element={<Layout />}>

              <Route path="/employeesList" element={<EmployeesList />} />
              <Route path="/plans" element={<Plans />} />

              <Route path="/product" element={<Prodcut />} />
              <Route path="/user" element={<User />} />
              <Route path="/withdraws" element={<Withdraws />} />
            </Route>
          </Routes>
        </BrowserRouter> :
          <LoginPage />

      }




    </div>
  )
}

export default App;
