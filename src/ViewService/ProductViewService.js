import ProductServices from "../Services/ProductServices";
import { errorMessage } from "../utils/alert/alert";
import { useDispatch } from "react-redux";
const productservices = new ProductServices();

const ProductViewService = () => {

    const dispatch = useDispatch();
    const SearchAllProduct = async () => {
        try {
            let list = await productservices.searchAllProduct();
            dispatch({ type: "setProductListModel", payload: list });
        }
        catch (err) {
            errorMessage(err.message);
        }
    }


    return { SearchAllProduct }
}

export default ProductViewService;