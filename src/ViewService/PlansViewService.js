import { useDispatch } from "react-redux";
import PlansServices from "../Services/PlansServices";
import mainStore from "../Stores/Redux/MainStore";
import { errorMessage, successMessage } from "../utils/alert/alert";

const plansServices = new PlansServices();

const PlansViewService = () => {
    const dispatch = useDispatch();
    const SearchAllPlans = async () => {
        try {
            let list = await plansServices.searchAllPlans();
            dispatch({ type: "SearchAllPlans", payload: list });
        }
        catch (err) {
            errorMessage(err.message);
        }
    }
    const RegisterPlans = async () => {
        const { plansModel } = mainStore.getState().plans;
        try {
            if (plansModel.id === 0) {
                const plans = await plansServices.registerPlans(plansModel);
                plansModel.id = plans.id;
                dispatch({ type: "registerPlans", payload: plans });

                successMessage();
            }
        }
        catch (err) {
            errorMessage(err.message);
        }
    }

    const IsActivePlans = async (id) => {
        try {
            let isAct = await plansServices.isActivePlans(id);

            dispatch({ type: "setisAvtive", payload: isAct });

        }
        catch (err) {
            errorMessage(err.message);
        }
    }



    return { SearchAllPlans, RegisterPlans, IsActivePlans }
}
export default PlansViewService;