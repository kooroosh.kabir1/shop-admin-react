import { errorMessage, successMessage } from "../utils/alert/alert";
import { useDispatch } from "react-redux";
import UserServices from "../Services/UserServices";
const userServices = new UserServices();

const UserViewService = () => {

    const dispatch = useDispatch();
    const SearchAllUser = async () => {
        try {
            let list = await userServices.searchAllUsers();
            dispatch({ type: "setUserListModel", payload: list });

        }
        catch (err) {
            errorMessage(err.message);
        }
    }
    const DisableUsers = async (id) => {
        try {
            let dis = await userServices.disableUsers(id);
            dispatch({ type: "setDisable", payload: dis });
            successMessage();
        }
        catch (err) {
            errorMessage(err.message);
        }
    }
    const EnableUsers = async (id) => {
        try {
            let enb = await userServices.enableUsers(id);
            dispatch({ type: "setDisable", payload: enb });
            successMessage();
        }
        catch (err) {
            errorMessage(err.message);
        }
    }

    return { SearchAllUser, DisableUsers, EnableUsers }
}

export default UserViewService;