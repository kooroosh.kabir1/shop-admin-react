import { errorMessage } from "../utils/alert/alert";
import { useDispatch } from "react-redux";
import EmployeesServices from "../Services/EmployeesServices"
const employeeservice = new EmployeesServices();

const EmployeeViewService = () => {

    const dispatch = useDispatch();
    const SearchAllEmployees = async () => {
        try {
            let list = await employeeservice.searchAllEmployees();
            dispatch({ type: "setEmployeesListModel", payload: list });
        }
        catch (err) {
            errorMessage(err.message);
        }
    }


    return { SearchAllEmployees }
}

export default EmployeeViewService;