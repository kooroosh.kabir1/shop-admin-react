import { useDispatch } from "react-redux"
import WithdrawsServices from "../Services/WithdrawsServices";
import { errorMessage } from "../utils/alert/alert";


const withdrawsService = new WithdrawsServices();
const WithdrawsViewService = () => {
    const dispatch = useDispatch();
    const SearchAllWithdraws = async () => {
        try {
            let list = await withdrawsService.searchAllWithraws();
            dispatch({ type: "setWihtdrawsListModel", payload: list });
        }
        catch (err) {
            errorMessage(err);
        }
    }
    const SearchAllWithrawsDone = async () => {
        try {
            let list = await withdrawsService.searchAllWithrawsDone();
            dispatch({ type: "setWihtdrawsListDone", payload: list });
        }
        catch (err) {
            errorMessage(err);
        }
    }
    const SearchAllWithrawsReject = async () => {
        try {
            let list = await withdrawsService.searchAllWithrawsReject();
            dispatch({ type: "setWihtdrawsListRegect", payload: list });
        }
        catch (err) {
            errorMessage(err);
        }
    }
    const WithrawsRejectAction = async (id) => {
        try {
            let Rej = await withdrawsService.withrawsRejectAction(id);
            dispatch({ type: "", payload: Rej });
        }
        catch (err) {
            errorMessage(err);
        }
    }
    const WithrawsDoneAction = async (id) => {
        try {
            let Rej = await withdrawsService.withrawsDoneAction(id);
            dispatch({ type: "", payload: Rej });
        }
        catch (err) {
            errorMessage(err);
        }
    }

    return { SearchAllWithdraws, WithrawsRejectAction, WithrawsDoneAction, SearchAllWithrawsReject, SearchAllWithrawsDone }
}
export default WithdrawsViewService;