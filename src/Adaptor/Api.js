import axios from "axios";
import ConfigViewService from "../ViewService/ConfigViewService";
import { AuthServerHandlerError, LoginRequiredError, ServerHandlerError } from "./ServerError";






const { showLoading, hideLoading } = ConfigViewService();

const baseURL = "";

const mainServer = axios.create({

    baseURL: "http://api.baghali-online.ir/api/v1/"
});

mainServer.defaults.headers.post = { "Content-Type": "application/json" };
mainServer.defaults.headers.put = { "Content-Type": "application/json" };

let token = localStorage.getItem("token");
mainServer.defaults.headers.common = {
    'Authorization': `Bearer ${token}`
}


mainServer.interceptors.request.use((cnf) => {
    showLoading();
    return cnf;
});

mainServer.interceptors.response.use((cnf) => {
    hideLoading();
    return cnf;
});


const authServer = axios.create({ baseURL: "http://api.baghali-online.ir/api/v1/" })
authServer.defaults.headers.post["Content-Type"] = "application/json";
authServer.defaults.headers.post["Access-Control-Allow-Origin"] = "*";


authServer.interceptors.request.use((cnf) => {
    showLoading();
    return cnf;
});

authServer.interceptors.response.use((cnf) => {
    hideLoading();
    return cnf;
});


export const Get = async (action, data = null) => {

    checkAccessToken();

    let token = localStorage.getItem("token");
    mainServer.defaults.headers.common = {
        'Authorization': `Bearer ${token}`
    }

    let params = "";
    if (data) {
        params = "?";
        for (let key in data) {
            params += key + "=" + data[key] + "&";
        }
    }


    let response = await mainServer.get(baseURL + action.url + params, /*{
        headers: { "Content-Type": "application/json" }
    }*/).catch(err => {

        throw new ServerHandlerError(err);
    });



    if (response)
        return response.data

    return null;
}

export const Post = async (action, data) => {
    checkAccessToken();

    let token = localStorage.getItem("token");
    mainServer.defaults.headers.common = {
        'Authorization': `Bearer ${token}`
    }

    let response = await mainServer.post(baseURL + action.url, data).catch(err => {
        throw new ServerHandlerError(err);
    });

    if (response)
        return response.data

    return null;
}

export const Put = async (action, data) => {
    checkAccessToken();

    let token = localStorage.getItem("token");
    mainServer.defaults.headers.common = {
        'Authorization': `Bearer ${token}`
    }

    let formData = new FormData();

    for (let key in data) {
        formData.append(key, data[key]);
    }


    let response = await mainServer.put(baseURL + action.url, formData).catch(err => {
        throw new ServerHandlerError(err);
    });

    if (response)
        return response.data

    return null;
}

export const Remove = async (action, ids) => {

    checkAccessToken();

    let token = localStorage.getItem("token");
    mainServer.defaults.headers.common = {
        'Authorization': `Bearer ${token}`
    }

    return await mainServer.delete(baseURL + action.url + "/" + Object.values(ids).join("/")).catch(err => {
        throw new ServerHandlerError(err);
    });
}


export const SignIn = async (userInfo) => {

    let formData = {};

    formData["username"] = userInfo.username;
    formData["password"] = userInfo.password;

    let response = await authServer.post("ManagerMain/login", formData)
        .catch(err => {
            throw new AuthServerHandlerError(err);
        });

    if (response && response.data) {
        let token = response.data["accessToken"];
        let accessTpokenExpireTime = response.data["accessTokenExpiration"];

        localStorage.setItem("accessTokenExpireTime", accessTpokenExpireTime);
        localStorage.setItem("token", token);
    }
    return null;
}
export const SignOut = async () => {

    let response = await authServer.get("/connect/endsession?id_token_hint=" + localStorage.getItem('token')).catch(err => {
        throw new AuthServerHandlerError(err);
    });
    localStorage.removeItem("token");
    return response.data;
}

function checkAccessToken() {

    let token = localStorage.getItem("token");

    let isValid = false;
    if (token) {
        return true;
    }
    if (!isValid) {
        throw new LoginRequiredError();
    }

    return true;
}

export function checkLogin() {

    let token = localStorage.getItem("token");
    if (token) {
        return true;
    }

    return false;
}




