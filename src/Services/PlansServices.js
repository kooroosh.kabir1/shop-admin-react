import { Get, Post } from "../Adaptor/Api";

export const getPlansLisitAction = { url: "MPlans", type: "get" };
export const getPlansRegisterAction = { url: "MPlans", type: "post" };
export const getPlansChangeAction = { url: "MPlans/", type: "post" };
export default class PlansServices {

    searchAllPlans() {
        return Get(getPlansLisitAction);
    }
    registerPlans(plan) {
        return Post(getPlansRegisterAction, plan);
    }
    isActivePlans(id) {
        return Post({ url: getPlansChangeAction.url + id + "/Change" });
    }


}