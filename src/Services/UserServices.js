

import { Get, Post } from "../Adaptor/Api";

export const getUsersListAction = { url: "MUsers", type: "get" };
export const getUsersListActionDisable = { url: "MUsers/", type: "post" };
export const getUsersListActionEnable = { url: "MUsers/", type: "post" };


export default class UserServices {

    searchAllUsers() {
        return Get(getUsersListAction);
    }
    disableUsers(id) {
        return Post({ url: getUsersListActionDisable.url + id + "/Disable" });
    }
    enableUsers(id) {
        return Post({ url: getUsersListActionEnable.url + id + "/Enable" });
    }

}