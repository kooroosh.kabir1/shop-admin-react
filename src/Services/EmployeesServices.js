import { Get, Remove } from "../Adaptor/Api";

export const getEmployeesListAction = { url: "Employees", type: "get" };
export const EmployeesDeleitAction = { url: "Employees/", type: "post" };
export default class EmployeesServices {

    searchAllEmployees() {
        return Get(getEmployeesListAction);
    }
    removeEmployee(id) {
        return Remove({ url: EmployeesDeleitAction.url + id });
    }
}
