import { Get, Post } from "../Adaptor/Api";


export const getWithrawsList = { url: "MWithdraws", type: "get" };
export const getWithrawsPendingList = { url: "MWithdraws?status=Pending", type: "get" };
export const getWithrawsRejectList = { url: "MWithdraws?status=Rejected", type: "get" };
export const getWithrawsDoneList = { url: "MWithdraws?status=Done", type: "get" };


//to reject a withdraw
export const WithrawsReject = { url: "MWithdraws/", type: "post" };

//to done a withdraw
export const WithrawsDone = { url: "MWithdraws/", type: "post" };
export default class WithdrawsServices {
    searchAllWithraws() {
        return Get(getWithrawsList);
    }
    searchAllWithrawsPending() {
        return Get(getWithrawsPendingList);

    }
    searchAllWithrawsReject() {
        return Get(getWithrawsRejectList);

    }
    searchAllWithrawsDone() {
        return Get(getWithrawsDoneList);

    }
    withrawsRejectAction(id) {
        return Post({ url: WithrawsReject.url + id + "/Reject?reason='dalile rad'" });
    }
    withrawsDoneAction(id) {
        return Post({ url: WithrawsDone.url + id + "/Done" });
    }
}