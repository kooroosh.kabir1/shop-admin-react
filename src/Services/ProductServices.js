import { Get } from "../Adaptor/Api";

export const getProductListAction = { url: "MProducts", type: "get" };
export default class ProductServices {

    searchAllProduct() {
        return Get(getProductListAction);
    }

}