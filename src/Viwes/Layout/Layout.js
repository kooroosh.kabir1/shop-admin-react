import React, { useState } from "react";
import { Stack } from "react-bootstrap";
import AppContent from "../Component/AppContent/AppContent";
import AppFooter from "../Component/AppFooter/AppFooter";
import AppHeader from "../Component/AppHeader/Appheader";
import SideBar from "../Component/SideBar/Sidebar";


const Layout = () => {

    const [showSideBar, setShowSidebar] = useState(true);
    const handelSideBar = () => {
        setShowSidebar(!showSideBar);
    }
    return (
        <>

            <Stack gap={1} className="main" style={{ marginRight: showSideBar ? 230 : 0 }}>

                <AppHeader toggleButtn={handelSideBar} />
                <AppContent />
                <AppFooter />
            </Stack>
            <SideBar show={showSideBar} />
        </>
    )
}
export default Layout;