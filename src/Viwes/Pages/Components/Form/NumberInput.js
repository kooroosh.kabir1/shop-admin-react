import { useEffect, useState } from 'react'

const NumberInput = ({ model, id, description, validation }) => {

    const [value, setvalue] = useState(model[id]);
    useEffect(() => {

        setvalue(model[id]);
    }, [model, id])
    const handleChange = (e) => {
        setvalue(e.target.value);
        model[id] = Number(e.target.value);
    }
    return (
        <>
            <label htmlFor={id}><b>{description}</b></label>
            <input {...validation} className="form-control" id={id} value={value} type="text" onChange={handleChange} />
        </>
    )
}
export default NumberInput;