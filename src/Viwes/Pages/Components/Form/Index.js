import TextInput from "./TextInput";
import NumberInput from "./NumberInput";
import SelecteInput from "./SelectInput";
import PasswordInput from "./PasswordInput";
export { TextInput, NumberInput, SelecteInput, PasswordInput };