
import { useEffect, useState } from 'react'
const SelecteInput = ({ model, id, description, list, text, validation }) => {

    const [value, setvalue] = useState(model[id]);
    useEffect(() => {

        setvalue(model[id]);
    }, [model, id])
    const handleChange = (e) => {
        setvalue(e.target.value);
        model[id] = Number(e.target.value);
    }
    return (
        <>
            <label htmlFor={id}><b>{description}</b></label>
            <select {...validation} value={value} id={id} onChange={handleChange}>

                {
                    list.map((val) => {
                        return <option key={val.id} value={val.id}>{val[text]}</option>
                    })
                }
            </select>
        </>
    )
}
export default SelecteInput;