import { useEffect, useState } from 'react'

const PasswordInput = ({ model, id, description, validation }) => {

    const [value, setvalue] = useState(model[id]);
    useEffect(() => {

        setvalue(model[id]);
    }, [model, id])
    const handleChange = (e) => {
        setvalue(e.target.value);
        model[id] = e.target.value;
    }
    return (
        <>
            <label htmlFor={id}><b>{description}</b></label>
            <input {...validation} className="form-control" id={id} value={value} type="password" onChange={handleChange} />
        </>
    )
}
export default PasswordInput;