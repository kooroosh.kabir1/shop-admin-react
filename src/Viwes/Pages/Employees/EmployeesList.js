import { AgGridReact, AgGridColumn } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

import './Employees.css';
import { useSelector } from 'react-redux';
import EmployeeViewService from '../../../ViewService/EmployeeViewService';





const EmployeesList = () => {

    const employeesListModel = useSelector(state => state.employee.employeesListModel);
    const { SearchAllEmployees } = EmployeeViewService();
    const onGridReady = () => {

        SearchAllEmployees();

        document.getElementsByClassName("ag-paging-row-summary-panel").item(0).childNodes.item(3).innerHTML = "از";
        document.getElementsByClassName("ag-paging-row-summary-panel").item(0).childNodes.item(7).innerHTML = "تا";
        document.getElementsByClassName("ag-paging-description").item(0).childNodes.item(1).innerHTML = "صفحه";
        document.getElementsByClassName("ag-paging-description").item(0).childNodes.item(5).innerHTML = "از";

    }

    return (

        <div className="EmployeesPage">

            <h4> لیست کارمندان</h4>
            <hr />
            <div className="ag-theme-alpine mt-4" style={{ height: 550, width: "35%" }}>
                <AgGridReact enableRtl pagination={true} paginationPageSize={10} rowSelection={'multiple'} rowMultiSelectWithClick={true}
                    rowClass="agRowStyle" rowData={employeesListModel} onGridReady={onGridReady} >

                    <AgGridColumn field="username" headerName="نام کاربر" />
                    <AgGridColumn field="fullName" headerName="نام کامل" />

                </AgGridReact>
            </div>

        </div>
    )
}
export default EmployeesList;