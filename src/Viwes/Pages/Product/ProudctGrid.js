import { AgGridColumn } from 'ag-grid-react';

// import { Link } from 'react-router-dom';
import ProductViewService from '../../../ViewService/ProductViewService';
import { useSelector } from 'react-redux';
import GridView from '../Components/GridView/GridView';

const ProductGrid = () => {


    const productListModel = useSelector(state => state.product.productListModel);
    const { SearchAllProduct } = ProductViewService();

    // const renderProdcutName = (params) => {
    //     return <Link to={"/product/" + params.data.id}>{params.data.productName}</Link>
    // }


    return <div className="ag-theme-alpine" style={{ height: 550, width: "100%" }}>
        <GridView listModel={productListModel} getData={SearchAllProduct}
        // frameworkComponents={{ renderprodcutName: renderProdcutName }} 
        >
            {/* <AgGridColumn field="imageUrl" headerName="عکس محصول" /> */}
            <AgGridColumn field="name" headerName="نام محصول" />
            <AgGridColumn field="sku" headerName=" کد محصول" />
            <AgGridColumn field="description" headerName="توضیحات" />
            <AgGridColumn field="weight" headerName="وزن" />
            <AgGridColumn field="price" headerName="قیمت" />
            <AgGridColumn field="discount" headerName="تخفیف" />
            <AgGridColumn field="quantity" headerName="تعداد" />
            <AgGridColumn field="category.name" headerName="دسته بندی" />
        </GridView>
    </div>
}

export default ProductGrid;

