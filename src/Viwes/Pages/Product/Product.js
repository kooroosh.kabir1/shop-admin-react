import { useState } from 'react';
import { Nav } from 'react-bootstrap'
import './product.css'

import ProductRegister from './ProductRegister';
import ProductCategories from './ProductCategories';


const Prodcut = () => {
    const [activeTab, setActiveTab] = useState(1);
    return (
        <div className="productPage">
            <h4>محصولات</h4>
            <hr />

            <Nav variant="tabs" defaultActiveKey={activeTab} style={{ direction: "rtl" }}>
                <Nav.Item>
                    <Nav.Link eventKey="1" onClick={() => { setActiveTab(1) }}>ثبت محصول</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="2" onClick={() => { setActiveTab(2) }}>دسته بندی محصولات</Nav.Link>
                </Nav.Item>

            </Nav>
            {
                activeTab === 1 ? <ProductRegister /> : <ProductCategories />
            }


        </div>



    )
}
export default Prodcut;