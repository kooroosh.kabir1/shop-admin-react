import { useState } from "react";
import { Nav } from "react-bootstrap";
import ListDoneWithdraws from "./ListDoneWithdraws";
import ListRegectWithdraws from "./ListRegectWithdraws";
import WithdrawsList from "./WithdrawsList";
import './Withdraw.css';
const Withdraws = () => {
    const [activeTab, setActiveTab] = useState(1);
    return (
        <div className="WithdrawsPage">
            <h4>تراکنش ها</h4>
            <hr />

            <Nav variant="tabs" defaultActiveKey={activeTab} style={{ direction: "rtl" }}>
                <Nav.Item>
                    <Nav.Link eventKey="1" onClick={() => { setActiveTab(1) }}>لست تراکنش ها</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="2" onClick={() => { setActiveTab(2) }}>لیست تراکنش های در حال انجام</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="3" onClick={() => { setActiveTab(3) }}>لیست تراکنش های رد شده</Nav.Link>
                </Nav.Item>
            </Nav>
            {
                activeTab === 1 ? <WithdrawsList /> : activeTab === 2 ? <ListDoneWithdraws /> : <ListRegectWithdraws />
            }


        </div>



    )
}
export default Withdraws;