import { Col, Container, Row } from "react-bootstrap";
import WithdrawsGrid from "./WithdrawsGrid";


const WithdrawsList = () => {
    return (
        <Container fluid>
            <div>
                <p className="mt-3">لیست تراکنش ها</p>
            </div>
            <hr />
            <Row>
                <Col>
                    <WithdrawsGrid />
                </Col>
            </Row>
        </Container>
    )
}
export default WithdrawsList;