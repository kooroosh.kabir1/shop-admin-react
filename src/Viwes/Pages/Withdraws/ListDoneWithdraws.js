import { Container } from "react-bootstrap";
import { AgGridColumn } from 'ag-grid-react';
import GridView from "../Components/GridView/GridView";
import WithdrawsViewService from "../../../ViewService/WithdrawsViewService";
import { useSelector } from "react-redux";

const ListDoneWithdraws = () => {
    const wihtdrawsListDone = useSelector(state => state.withdraws.WihtdrawsListDone);
    const { SearchAllWithrawsDone } = WithdrawsViewService();
    return (
        <Container fluid>
            <div>
                <p className="mt-3">لیست تراکنش های تایید شده</p>
            </div>
            <hr />


            <div className="ag-theme-alpine" style={{ height: 550, width: "85%" }}>
                <GridView listModel={wihtdrawsListDone} getData={SearchAllWithrawsDone} 

                >
                    <AgGridColumn field="fullName" headerName="تراکنش ها" />
                    <AgGridColumn field="cardNumber" headerName="تراکنش های " />
                    <AgGridColumn field="amount" headerName="تراکنش ها" />
                    <AgGridColumn field="status" headerName="تراکنش ها" />


                </GridView>
            </div>

        </Container>
    )
}
export default ListDoneWithdraws;