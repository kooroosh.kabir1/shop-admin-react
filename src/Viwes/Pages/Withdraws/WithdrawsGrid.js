import { AgGridColumn } from 'ag-grid-react';
import { useSelector } from 'react-redux';
import WithdrawsViewService from '../../../ViewService/WithdrawsViewService';

// import { Link } from 'react-router-dom';

import GridView from '../Components/GridView/GridView';

const WithdrawsGrid = () => {

    const WihtdrawsListModel = useSelector(state => state.withdraws.WihtdrawsListModel);
    const { SearchAllWithdraws, WithrawsRejectAction, WithrawsDoneAction } = WithdrawsViewService();


    const renderbtn = (params) => {
        return (<span>
            <i className="fa fa-check" style={{ fontSize: 30, color: 'green' }} onClick={() => { WithrawsDoneAction(params.data.id) }}></i>
            <i className="fa fa-close" style={{ fontSize: 30, color: 'red', paddingRight: 20 }} onClick={() => { WithrawsRejectAction(params.data.id) }}></i>
        </span>)
    }


    return <div className="ag-theme-alpine" style={{ height: 550, width: "85%" }}>
        <GridView listModel={WihtdrawsListModel} getData={SearchAllWithdraws}
            frameworkComponents={{ renderbtn: renderbtn }}
        >
            <AgGridColumn field="fullName" headerName="تراکنش ها" />
            <AgGridColumn field="cardNumber" headerName="تراکنش های " />
            <AgGridColumn field="amount" headerName="تراکنش ها" />
            <AgGridColumn field="status" headerName="تراکنش ها" />
            <AgGridColumn headerName=" تغییر وضعیت" cellRenderer="renderbtn" />

        </GridView>
    </div>
}

export default WithdrawsGrid;

