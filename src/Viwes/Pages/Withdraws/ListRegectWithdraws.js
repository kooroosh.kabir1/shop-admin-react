import { Container } from "react-bootstrap";

import { AgGridColumn } from 'ag-grid-react';
import GridView from "../Components/GridView/GridView";
import WithdrawsViewService from "../../../ViewService/WithdrawsViewService";
import { useSelector } from "react-redux";
const ListRegectWithdraws = () => {
    const wihtdrawsListRegect = useSelector(state => state.withdraws.WihtdrawsListRegect);
    const { SearchAllWithrawsReject } = WithdrawsViewService();
    return (
        <Container fluid>
            <div>
                <p className="mt-3">لیست تراکنش های تایید شده</p>
            </div>
            <hr />


            <div className="ag-theme-alpine" style={{ height: 550, width: "85%" }}>
                <GridView listModel={wihtdrawsListRegect} getData={SearchAllWithrawsReject}

                >
                    <AgGridColumn field="fullName" headerName="تراکنش ها" />
                    <AgGridColumn field="cardNumber" headerName="تراکنش های " />
                    <AgGridColumn field="amount" headerName="تراکنش ها" />
                    <AgGridColumn field="status" headerName="تراکنش ها" />


                </GridView>
            </div>

        </Container>
    )
}
export default ListRegectWithdraws;