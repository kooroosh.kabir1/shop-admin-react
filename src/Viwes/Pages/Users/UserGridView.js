

import { AgGridColumn } from 'ag-grid-react';

// import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import UserViewService from '../../../ViewService/UserViewService';
import GridView from '../Components/GridView/GridView';

const UserGridView = () => {

    const userListModel = useSelector(state => state.user.userListModel);
    const { SearchAllUser, DisableUsers, EnableUsers } = UserViewService();



    const renderBtn = (params) => {
        return (<span  >
            <i className="fa fa-check " style={{ fontSize: 20, color: 'green' }} onClick={() => { EnableUsers(params.data.id) }}></i>
            <i className="	fa fa-close" style={{ fontSize: 20, color: 'red', paddingRight: 30 }} onClick={() => { DisableUsers(params.data.id) }}></i>
        </span>)

    }

    return <div className="ag-theme-alpine" style={{ height: 550, width: "45%", textAlign: "center" }}>
        <GridView listModel={userListModel} getData={SearchAllUser} className="m-2!import" frameworkComponents={{ renderBtn: renderBtn }}>

            <AgGridColumn field="fullName" headerName="نام کاربر" />
            <AgGridColumn field="status" headerName="وضعیت" />
            <AgGridColumn headerName="وضعیت فعالیت" cellRenderer="renderBtn" />
        </GridView>
    </div>
}

export default UserGridView;

