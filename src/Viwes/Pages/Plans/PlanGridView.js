

import { AgGridColumn } from 'ag-grid-react';

import { useSelector } from 'react-redux';
import GridView from '../Components/GridView/GridView';
import PlansViewService from "../../../ViewService/PlansViewService";
import { successMessage } from '../../../utils/alert/alert';
import { Link } from 'react-router-dom';
const PlanGridView = () => {

    const planListModel = useSelector(state => state.plans.plansListModel);
    const { SearchAllPlans, IsActivePlans } = PlansViewService();


    const renderBtn = (params) => {
        return (

            <span className="fa fa-close" style={{ color: 'red', fontSize: 30 }} onClick={async () => {
                await IsActivePlans(params.data.id);
                successMessage();
            }}></span>


        )

    }

    return <div className="ag-theme-alpine" style={{ height: 550, width: "100%" }}>

        <GridView listModel={planListModel} getData={SearchAllPlans}
            frameworkComponents={{ renderBtn: renderBtn }} >



            <AgGridColumn field="title" headerName="نام پلن" />
            <AgGridColumn field="price" headerName="قیمت" />
            <AgGridColumn field="discount" headerName="تخفیف" />
            <AgGridColumn field="netPrice" headerName="قیمت خالص" />
            <AgGridColumn field="subscriptionDays" headerName="مدت اشتراک" />
            <AgGridColumn field="description" headerName="توضیحات" />
            <AgGridColumn field="isActive" headerName="وضعیت" />
            <AgGridColumn headerName="تغییر وضعیت" cellRenderer="renderBtn" />
        </GridView>
    </div>
}

export default PlanGridView;

