import { TextInput, NumberInput } from '../Components/Form/Index';
import './Plans.css'
import PlanGridView from './PlanGridView';
import { Container, Row, Col } from 'react-bootstrap';
import { Modal, Button } from 'react-bootstrap';
import { useState } from "react";
import { useSelector } from 'react-redux';
import PlansViewService from '../../../ViewService/PlansViewService';



const RegisterPlans = () => {
    const [show, setShow] = useState(false);
    const plansModel = useSelector(state => state.plans.plansModel);

    const { RegisterPlans } = PlansViewService();

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <div className="PlansPage">


                <Button variant="success" className='mt-3' onClick={handleShow}>
                    <span className='fa fa-plus' style={{ marginLeft: 5 }}></span>
                    ثبت پلن

                </Button>
                <Modal className="text-dark "
                    size="lg"
                    show={show}
                    onHide={handleClose}
                    aria-labelledby="example-modal-sizes-title-lg"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="example-modal-sizes-title-lg">
                            ثبت پلن
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {
                            <Container fluid className="pt-2">
                                <Row>
                                    <Col>

                                        <TextInput id="title" model={plansModel} description="نام" />
                                    </Col>
                                    <Col>

                                        <NumberInput model={plansModel} id="price" description="قیمت" />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>

                                        <NumberInput model={plansModel} id="discount" description="تخفیف" />
                                    </Col>
                                    <Col>

                                        <NumberInput model={plansModel} id="shippingDays" description="مدت اشتراک" />
                                    </Col>
                                </Row>
                                {/* <Row>
                                        <Col>
                                            <label htmlFor=""> </label>
                                            <input type="text" className="form-control mt-1" />
                                        </Col>
                                        <Col>
                                            <label htmlFor=""> </label>
                                            <input type="text" className="form-control mt-1" />
                                        </Col>
                                    </Row> */}
                                <Row>
                                    <Col>

                                        <TextInput id="description" model={plansModel} description="توضیحات" />
                                    </Col>
                                </Row>

                            </Container>
                        }
                    </Modal.Body>
                    <hr />
                    <Button variant="success" onClick={async () => {

                        await RegisterPlans();
                        handleClose();
                        window.location.reload();
                    }} style={{ width: 100, paddingLeft: 10 }}>ثبت</Button>
                    <br />
                </Modal>
                <div className="mt-4">
                    <PlanGridView />

                </div>
            </div>
        </>
    )
}
export default RegisterPlans;