import { useEffect } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useParams } from 'react-router-dom';
import PlansViewService from '../../../ViewService/PlansViewService';

const EditPlans = () => {
    const navigate = useNavigate();
    const { plansModel } = useSelector(state => state.plans.plansModel);
    
    const { RegisterPlans, EditPlans } = PlansViewService();
    const { plansid } = useParams();
    useEffect(() => {

        if (plansid) {
            EditPlans(plansid);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [plansid]);
    return (
        <div className="PlansPage">
            <h4>ویرایش پلن ها  </h4>
            <hr />

            <Container style={{ height: 600 }}>
                <form className="p-5 border bg-light " >
                    <Row>
                        <Col>

                            {/* <TextInput model={plansModel} id="title" description="نام" /> */}
                            <label htmlFor="title"><b>نام:</b></label>
                            <input className="form-control" id="title" type="text" />
                        </Col>
                        <Col> {/* <NumberInput model={plansModel} id="price" description="قیمت" /> */}
                            <label htmlFor="price"><b>قیمت:</b></label>
                            <input className="form-control" id="price" type="number" model={plansModel} />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <label htmlFor="discount"><b>تخفیف:</b></label>
                            <input className="form-control" id="discount" type="number" model={plansModel} />
                            {/* <NumberInput model={plansModel} id="discount" description="تخفیف" /> */}
                        </Col>
                        <Col>
                            <label htmlFor="shippingDays"><b>مدت اشتراک:</b></label>
                            <input className="form-control" id="shippingDays" type="number" model={plansModel} />

                            {/* <NumberInput model={plansModel} id="shippingDays" description="مدت اشتراک" /> */}
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <label htmlFor="description"><b>توضیحات:</b></label>
                            <textarea className="form-control" id="description" type="text" model={plansModel} style={{ height: 100 }} />
                            {/* <TextInput model={plansModel} id="description" description="توضیحات" /> */}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <button className="btn btn-success mt-2" style={{ width: 150, float: "left", }} onClick={async () => {
                                await RegisterPlans();
                                if (plansModel.id)
                                    navigate("/plans" + plansModel.id);
                            }}>ثبت</button>
                        </Col>
                        <Col>
                            <button className="btn btn-danger mt-2" style={{ width: 150, float: "right", }} onClick={() => {
                                navigate("/plans")
                            }}> بازگشت</button>
                        </Col>
                    </Row>
                </form>
            </Container>
        </div>
    )
}
export default EditPlans;