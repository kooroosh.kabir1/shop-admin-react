import { Container, Dropdown, Navbar, NavbarBrand } from "react-bootstrap";
import DropdownItem from "react-bootstrap/esm/DropdownItem";
import DropdownMenu from "react-bootstrap/esm/DropdownMenu";
import DropdownToggle from "react-bootstrap/esm/DropdownToggle";
import AccountViewService from "../../../ViewService/AccountViewService";



const AppHeader = ({ toggleButtn }) => {
    const { logout } = AccountViewService();
    return (
        <Navbar bg="light" expand={false}>
            <Container fluid style={{ direction: "ltr" }}>
                <Dropdown style={{}} className="user-profile">
                    <DropdownToggle>
                        <button className="btn text-light" style={{ fontSize: 12, padding: 0 }}>ثبت نام</button>
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem href="#" >ثبت نام</DropdownItem><hr />
                        <DropdownItem href="#" onClick={logout} >خروج</DropdownItem>
                    </DropdownMenu>
                </Dropdown>
                <NavbarBrand href="#"></NavbarBrand>
                <Navbar.Toggle aria-controls="offcanvasNavbar" onClick={toggleButtn} />
            </Container>

        </Navbar>
    )
}
export default AppHeader;