import { useContext } from "react";
import { MenuContext } from "../../../Stores/Contexts/MenuContext";
import MenuItem from "./MenuItem";


const Menu = () => {
    const { menuList } = useContext(MenuContext);
    return <div className="menu_section">
        <ul className="nav side-menu page-sidebar-menu side-show ">
            {
                menuList && menuList.map((value, index) => {
                    return (<MenuItem title={value.title} index={index} key={index} subMenus={value.subMenus} icon={value.icon} />)
                })

            }
        </ul>
    </div>
}
export default Menu;