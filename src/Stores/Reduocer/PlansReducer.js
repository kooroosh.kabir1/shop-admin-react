const initialPlans = {
    plansListModel: [],
    plansModel: { id: 0, title: "", price: 0, discount: 0, shippingDays: 0, description: "" },
    isActive: true,

}
const PlansReducer = (state = initialPlans, action) => {

    switch (action.type) {
        case "SearchAllPlans":
            {
                return { ...state, plansListModel: action.payload };
            }
        case "registerPlans":
            {
                return { ...state, plansModel: action.payload };
            }
        case "setisAvtivePlans":
            {
                return { ...state, isActive: action.payload };
            }
        case "setisAvtive":
            {
                return { ...state, isActive: false };
            }
        case "newPlans":
            {
                return { ...state, plansModel: { id: 0, title: "", price: 0, discount: 0, shippingDays: 0, description: "" } };
            }

        default:
            return state;
    }
}
export default PlansReducer;