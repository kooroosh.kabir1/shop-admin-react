const initialUser = {
    userListModel: [],statusListModel:[],
    disable:false,enable:true,
}

const UserReducer = (state = initialUser, action) => {

    switch (action.type) {
        case "setUserListModel":
            {
                return { ...state, userListModel: action.payload }
            }
            case "setStatusListModel":
                {
                    return { ...state, statusListModel: action.payload }
                }
                case "setDisable":
                {
                    return { ...state, disable:true }
                }
                case "setEnable":
                {
                    return { ...state, enable:false }
                }

        default:
            return state;
    }
}
export default UserReducer;