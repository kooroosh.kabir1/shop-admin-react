const initialEmployeesstate = {
    employeesListModel: [],
    removEmployee:{}
}

const EmployeesReduocer = (state = initialEmployeesstate, action) => {
    switch (action.type) {
        case "setEmployeesListModel":
            {
                return { ...state, employeesListModel: action.payload }
            }


        default:
            return state;
    }
}
export default EmployeesReduocer;