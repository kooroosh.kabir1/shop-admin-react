import { createContext, useState } from "react";

export const MenuContext = createContext(null);

const MenuProvider = ({ children }) => {

    const menuList = [
        {
            title: "داشبورد", icon: "fa fa-home",
            subMenus: [{ title: 'صفحه اصلی', icon: "fa fa-home", to: "#" }]

        }

        ,
        {
            title: 'محصولات', icon: 'fa fa-product-hunt',
            subMenus: [{ title: ' مدیریت محصولات', icon: "fa fa-product-hunt", to: "Product" }

            ]


        },
        {
            title: 'کارمندان', icon: 'fa fa-user-o',
            subMenus: [
                { title: 'لیست کارمندان', icon: "fa fa-list-alt", to: "employeesList" },
            ]


        },
        {
            title: 'پلن ها', icon: 'fa fa-slideshare',
            subMenus: [{ title: 'لیست پلن ها', icon: "	fa fa-list-alt", to: "plans" },
            { title: 'ویرایش پلن ها', icon: "fa fa-edit", to: "editPlans" },]


        },
        {
            title: 'کاربران', icon: 'fa fa-user-o',
            subMenus: [{ title: 'لیست  کاربران', icon: "fa fa-list-alt", to: "user" },]


        },
        {
            title: 'تراکنش ها', icon: 'fa fa-user-o',
            subMenus: [{ title: 'لیست  تراکنش ها', icon: "fa fa-list-alt", to: "withdraws" },]


        },
        { title: 'راهنما', icon: 'fa fa-question' }
    ];

    const [activeMenu, setActiveMenu] = useState(-1);

    const selectMenu = (index) => {
        if (index === activeMenu) {
            setActiveMenu(-1);
        }
        else
            setActiveMenu(index);
    }

    return (<MenuContext.Provider value={{ selectMenu, activeMenu, menuList }}>
        {children}
    </MenuContext.Provider>)
}

export default MenuProvider;