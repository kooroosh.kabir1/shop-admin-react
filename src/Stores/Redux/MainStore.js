import { createStore, combineReducers } from 'redux'
import AccountReducer from '../Reduocer/AccountReducer';
import EmployeesReduocer from '../Reduocer/EmployeesReduocer';
import ProdcutReduocer from '../Reduocer/ProductReduocer';
import ConfigReducer from '../Reduocer/ConfigReducer';
import PlansReducer from '../Reduocer/PlansReducer';
import UserReducer from '../Reduocer/UserReducer';
import WithdrawsReducer from '../Reduocer/WithdrawsReducer';

const reducers = combineReducers({
    product: ProdcutReduocer, employee: EmployeesReduocer, account: AccountReducer,
    config: ConfigReducer, plans: PlansReducer, user: UserReducer, withdraws: WithdrawsReducer
});

const mainStore = createStore(reducers);

export default mainStore;